% NETR(1) net user manual
% Ed Neville (ed-netr@s5h.net)
% 07 Dec 2020

# NAME

netr - a simple tool to display network throughput

# SYNOPSIS

```
netr
```

# DESCRIPTION

netr shows a list of network interfaces and their throughput per second and minute. There are no current options for this command, it is meant to be simple.

Input (%) and output (#) are separated in the graph.

Exit by killing the process with ctrl-c.

# CONTRIBUTIONS

I welcome pull requests with open arms.

